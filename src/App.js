// @flow
import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import { Provider as StoreProvider } from 'react-redux';
import { IconRegistry } from 'react-native-ui-kitten';
import { EvaIconsPack } from '@ui-kitten/eva-icons';

import ThemeConnectedProvider from './ThemeConnectedProvider';

import StatusBar from './components/StatusBar';

import MainTabs from './navigation/MainTabs';

import store from './store';

const App = () => (
  <>
    <IconRegistry icons={EvaIconsPack} />
    <StoreProvider store={store}>
      <ThemeConnectedProvider>
        <SafeAreaView style={styles.safeArea}>
          <StatusBar />
          <MainTabs />
        </SafeAreaView>
      </ThemeConnectedProvider>
    </StoreProvider>
  </>
);

export default App;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
});
