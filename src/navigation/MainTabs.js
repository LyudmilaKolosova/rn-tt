import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import FeedScreen from '../screens/Feed/Feed.container';
import ChatScreen from '../screens/Chat/Chat.container';
import SettingsScreen from '../screens/Settings/Settings.container';

const TabNavigator = createBottomTabNavigator({
  Feed: FeedScreen,
  Chat: ChatScreen,
  Settings: SettingsScreen,
});

export default createAppContainer(TabNavigator);
