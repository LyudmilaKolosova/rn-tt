// @flow
import React, { Component } from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import type { IChatMessage } from 'react-native-gifted-chat/lib/types.js.flow';

type Props = {};

type Messages = Array<IChatMessage>;

type State = {
  messages: Messages,
};

export default class ChatScreen extends Component<Props, State> {
  state = {
    messages: [
      {
        _id: 1,
        text: 'Hello developer',
        createdAt: new Date(),
        user: {
          _id: 2,
          name: 'React Native',
          avatar: 'https://placeimg.com/140/140/any',
        },
      },
    ],
  };

  onSend = (message: Messages = []) => {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, message),
    }));
  };

  render() {
    const { messages } = this.state;

    return (
      <GiftedChat
        messages={messages}
        onSend={this.onSend}
        user={{
          _id: 1,
        }}
      />
    );
  }
}
