import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { fetchRedditFeed } from '../../store/ducks/feed/actions';
import { list, loading } from '../../store/ducks/feed/selectors';

import Feed from './Feed';

const mapStateToProps = createStructuredSelector({
  list,
  loading,
});

const mapDispatchToProps = {
  fetchRedditFeed,
};

export default connect(mapStateToProps, mapDispatchToProps)(Feed);
