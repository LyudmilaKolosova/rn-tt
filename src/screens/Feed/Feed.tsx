// @flow
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import { Layout, List, Spinner, TopNavigation } from 'react-native-ui-kitten';

import FeedListItem from '../../components/FeedListItem';

import { Feed } from '../../store/ducks/feed/reducer';

type Props = {
  list: Array<Feed>,
  loading: boolean,
  fetchRedditFeed: Function,
};

export default class FeedScreen extends Component<Props> {
  componentDidMount() {
    const { fetchRedditFeed } = this.props;

    fetchRedditFeed();
  }

  renderItem = ({ item }: { item: Feed }) => (
    <FeedListItem
      title={`/${item.subredditNamePrefixed}`}
      description={item.title}
      ups={item.ups}
      numComments={item.numComments}
      thumbnail={item.thumbnail}
    />
  );

  renderSpinner = () => {
    const { loading } = this.props;

    if (loading) {
      return (
        <View style={styles.spinnerContainer}>
          <Spinner />
        </View>
      );
    }

    return null;
  };

  render() {
    const { list } = this.props;

    return (
      <Layout style={styles.container}>
        <TopNavigation title="Popular" alignment="center" />
        <List
          style={styles.list}
          data={list}
          renderItem={this.renderItem}
          ListFooterComponent={this.renderSpinner}
        />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    flex: 1,
  },
  spinnerContainer: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
