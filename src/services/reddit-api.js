import Config from 'react-native-config';
import axios from 'axios';

const api = axios.create({
  baseURL: Config.API_BASE_URL,
  timeout: 3000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

export const getPopularFeed = () => api.get('/r/popular.json?limit=10');
