import {
  applyMiddleware,
  compose,
  createStore as createReduxStore,
} from 'redux';
import thunk from 'redux-thunk';

import Reactotron from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';

const middlewares = [thunk];

export default function createStore(reducers) {
  // eslint-disable-next-line no-console
  console.tron = Reactotron;

  Reactotron.configure()
    .useReactNative()
    .use(reactotronRedux())
    .connect();

  return createReduxStore(
    reducers,
    compose(applyMiddleware(...middlewares), Reactotron.createEnhancer()),
  );
}
