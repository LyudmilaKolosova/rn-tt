// @flow
import type { State } from '../..';

export const list = (state: State) => state.feed.list;
export const loading = (state: State) => state.feed.loading;
export const error = (state: State) => state.feed.error;
