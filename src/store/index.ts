// @flow
import createStore from './createStore';

import reducers from './ducks';

import type { State as FeedState } from './ducks/feed/reducer';
import type { State as SettingsState } from './ducks/settings/reducer';

export type State = {
  feed: FeedState,
  settings: SettingsState,
};

export type Action = {
  type: string,
  payload?: any,
};

export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type Dispatch = (action: Action | ThunkAction) => any;
export type GetState = () => State;

export default createStore(reducers);
