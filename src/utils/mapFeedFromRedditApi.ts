// @flow
import type { Feed } from '../store/ducks/feed/reducer';

export default function mapFeedFromRedditApi(feed: Object): Feed {
  const { data } = feed;

  return {
    id: data.id,
    authorFullname: data.author_fullname,
    author: data.author,
    created: data.created,
    downs: data.downs,
    numComments: data.num_comments,
    subredditId: data.subreddit_id,
    subredditNamePrefixed: data.subreddit_name_prefixed,
    subreddit: data.subreddit,
    thumbnail: data.thumbnail.includes('http') ? data.thumbnail : null,
    title: data.title,
    totalAwardsReceived: data.total_awards_received,
    ups: data.ups,
    url: data.url,
  };
}
